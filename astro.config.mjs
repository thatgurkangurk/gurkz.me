import { defineConfig } from 'astro/config';

// https://astro.build/config
import prefetch from "@astrojs/prefetch";
import unocss from 'unocss/astro';

// https://astro.build/config
import react from "@astrojs/react";

// https://astro.build/config
import vercel from "@astrojs/vercel/serverless";

// https://astro.build/config
export default defineConfig({
  site: 'https://gurkz.me',
  integrations: [prefetch(), unocss(), react()],
  output: "server",
  adapter: vercel()
});