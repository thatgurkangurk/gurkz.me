import HeroPost from "./hero-post";
import MoreStories from "./more-stories";

export default function PostList({ allPosts: { edges } }: any) {
    const heroPost = edges[0]?.node;
    const morePosts = edges.slice(1);

    return (
        <>
            {heroPost && (
                <HeroPost
                    title={heroPost.title}
                    coverImage={heroPost.featuredImage}
                    date={heroPost.date}
                    author={heroPost.author}
                    slug={heroPost.slug}
                    excerpt={heroPost.excerpt}
                />
            )}
            {morePosts.length > 0 && <MoreStories posts={morePosts} />}
        </>
    )
}