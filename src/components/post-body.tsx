export default function PostBody({ content }: { content: string }) {
  return (
    <div className={`prose lg:prose-lg`}>
      <div
        dangerouslySetInnerHTML={{ __html: content }}
      />
    </div>
  )
}
