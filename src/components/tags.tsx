import type { ReactElement, JSXElementConstructor, ReactFragment, ReactPortal, Key } from "react";

export default function Tags({ tags }: any) {
    return (
      <div className="max-w-2xl mx-auto">
        <p className="mt-8 text-lg font-bold">
          Tagged
          {tags.edges.map((tag: { node: { name: string | number | boolean | ReactElement<any, string | JSXElementConstructor<any>> | ReactFragment | ReactPortal | null | undefined; }; }, index: Key | null | undefined) => (
            <span key={index} className="ml-4 font-normal">
              {tag.node.name}
            </span>
          ))}
        </p>
      </div>
    )
  }
  