import {
    defineConfig, presetUno, transformerDirectives, transformerVariantGroup
} from 'unocss';
import {
    presetTypography
} from '@unocss/preset-typography';

export default defineConfig({
    presets: [
        presetUno(),
        presetTypography()
    ],
    transformers: [transformerDirectives(), transformerVariantGroup()],
    theme: {
        fontFamily: {
            satoshi: "Satoshi",
        },
        colors: {
            themeColor: "#119DA4",
            themeBlack: "#121212",
        }
    },
})